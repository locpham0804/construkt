import React, { useEffect, useMemo, useLayoutEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';

import { Button, LogBox, Text, View, ActivityIndicator } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Playground from './screens/Playground';
import RootStackScreen from './screens/RootStackScreen';
import Home from './screens/Home'
import { DrawerContent } from './screens/DrawerContent';
import 'react-native-gesture-handler';
import { AuthContext } from './components/context';
import AsyncStorage from '@react-native-async-storage/async-storage'
import jwtDecode from "jwt-decode";
import { 
  Provider as PaperProvider, 
  DefaultTheme as PaperDefaultTheme,
  DarkTheme as PaperDarkTheme 
} from 'react-native-paper';
import { 
  DefaultTheme as NavigationDefaultTheme,
  DarkTheme as NavigationDarkTheme
} from '@react-navigation/native';
import {useAuth0, Auth0Provider} from 'react-native-auth0';

// TODO: Remove when fixed
LogBox.ignoreLogs([
  (window.performance.clearMeasures = () => { }),
  (window.performance.clearMarks = () => { }),
  (window.performance.measure = () => { }),
  (window.performance.mark = () => { }),
]);

function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        onPress={() => navigation.navigate('Notifications')}
        title="Go to notifications"
      />
    </View>
  );
}

function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}

const Drawer = createDrawerNavigator();

export default function App() {

  const [isDarkTheme, setIsDarkTheme] = React.useState(false);

  const initialLoginState = {
    isLoading: true,
    userInfo: null,
    userToken: null,
    role: null
  };

  const CustomDefaultTheme = {
    ...NavigationDefaultTheme,
    ...PaperDefaultTheme,
    colors: {
      ...NavigationDefaultTheme.colors,
      ...PaperDefaultTheme.colors,
      background: '#ffffff',
      text: '#333333'
    }
  }

  const CustomDarkTheme = {
    ...NavigationDarkTheme,
    ...PaperDarkTheme,
    colors: {
      ...NavigationDarkTheme.colors,
      ...PaperDarkTheme.colors,
      background: '#333333',
      text: '#ffffff'
    }
  }

  const theme = isDarkTheme ? CustomDarkTheme : CustomDefaultTheme;

  const loginReducer = (prevState, action) => {
    console.log(action)
    switch (action.type) {
      case 'RETRIEVE_TOKEN':
        return {
          ...prevState,
          userToken: action.info,
          role: action.role,
          isLoading: false,
        };
      case 'LOGIN':
        return {
          ...prevState,
          userInfo: action.info,
          userToken: action.token,
          role: action.role,
          isLoading: false,
        };
      case 'LOGOUT':
        return {
          ...prevState,
          userInfo: null,
          userToken: null,
          role: null,
          isLoading: false,
        };
      case 'TRIAL':
        return {
          ...prevState,
          userInfo: 'Test Account',
          userToken: 'Test Token',
          role: 'trial',
          isLoading: false,
        };
      case 'REGISTER':
        return {
          ...prevState,
          userInfo: action.info,
          userToken: action.token,
          isLoading: false,
        };
    }
  };

  const [loginState, dispatch] = React.useReducer(loginReducer, initialLoginState);

  const authContext = useMemo(() => ({
    signIn: async (foundUser) => {
      // setUserToken('fgkj');
      // setIsLoading(false);

      try {
        await AsyncStorage.setItem('userToken', JSON.stringify(foundUser));
      } catch (e) {
        console.log(e);
      }
      // console.log('user token: ', userToken);
      dispatch({ type: 'LOGIN', token: foundUser.sub, state: foundUser.email_verified, info: foundUser.email });
    },
    trialLogin: async () => {
      // setUserToken('fgkj');
      // setIsLoading(false);

      try {
        await AsyncStorage.setItem('userToken', 'Test Token');
      } catch (e) {
        console.log(e);
      }
      // console.log('user token: ', userToken);
      dispatch({ type: 'TRIAL' });
    },
    signOut: async () => {
      // setUserToken(null);
      // setIsLoading(false);
      try {
        await AsyncStorage.removeItem('userToken');
      } catch (e) {
        console.log(e);
      }
      dispatch({ type: 'LOGOUT' });
    },
    signUp: () => {
      // setUserToken('fgkj');
      // setIsLoading(false);
    },
    toggleTheme: () => {
      setIsDarkTheme(isDarkTheme => !isDarkTheme);
    }
  }), []);

  useEffect(() => {
    setTimeout(async () => {
      // setIsLoading(false);
      let userToken;
      userToken = null;
      try {
        userToken = await AsyncStorage.getItem('userToken');
      } catch (e) {
        console.log(e);
      }
      // console.log('user token: ', userToken);
      dispatch({ type: 'RETRIEVE_TOKEN', token: userToken });
    }, 1000);
  }, []);

  if (loginState.isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large"/>
      </View>
    );
  }

  return (
    <Auth0Provider domain={"dev-jsvve4ty.us.auth0.com"} clientId={"PhUH4TskV32YMloTPXyyomm44uBpZgR9"}>
    <PaperProvider theme={theme}>
      <AuthContext.Provider value={authContext}>
        <NavigationContainer theme={theme}>
            {loginState.userToken !== null && loginState.userToken !== undefined && loginState.role == 'student' ? (
              <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
                <Drawer.Screen name="Home" component={Home}/>
                <Drawer.Screen name="PlayGround" component={Playground}/>
              </Drawer.Navigator>
            ) : loginState.userToken !== null && loginState.userToken !== undefined && loginState.role == 'trial' ? (
              <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
                <Drawer.Screen name="Home" component={Home}/>
                <Drawer.Screen name="PlayGround" component={Playground}/>
              </Drawer.Navigator>
            )
              : loginState.userToken !== null && loginState.userToken !== undefined ? (
                <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
                  <Drawer.Screen name="Home" component={Home}/>
                  <Drawer.Screen name="PlayGround" component={Playground}/>
                </Drawer.Navigator>
              ) :
              (<RootStackScreen/>)
            }
        </NavigationContainer>
      </AuthContext.Provider>
    </PaperProvider>
    </Auth0Provider>
  );
}
