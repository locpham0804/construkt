import {useRef} from "react";
import Cube from "./Cube";
import { useFrame } from "@react-three/fiber/native";

export const LShapeBaseCube = {
    'C0': [0, 0, 0],
    'C1': [0, 1, 0],
    'C2': [0, 0, 1],
    'C3': [0, 0, 2]
}



export default function LShape(props) {
    const mesh = useRef();
    // useFrame(() => {
    //     if (mesh && mesh.current) {
    //         mesh.current.rotation.x = mesh.current.rotation.x += 0.1;
    //     }
    // });
    return (
        <mesh {...props} ref={mesh}>
            <Cube position={LShapeBaseCube.C0} name={{shape: {type: 'L', position: props.position, rotation: props.rotation}, cube: {type: 'C0', position: LShapeBaseCube.C0}}}/>
            <Cube position={LShapeBaseCube.C1} name={{shape: {type: 'L', position: props.position, rotation: props.rotation}, cube: {type: 'C1', position: LShapeBaseCube.C1}}}/>
            <Cube position={LShapeBaseCube.C2} name={{shape: {type: 'L', position: props.position, rotation: props.rotation}, cube: {type: 'C2', position: LShapeBaseCube.C2}}}/>
            <Cube position={LShapeBaseCube.C3} name={{shape: {type: 'L', position: props.position, rotation: props.rotation}, cube: {type: 'C3', position: LShapeBaseCube.C3}}}/>
        </mesh>
    )
}
