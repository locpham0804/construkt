import {useRef, useState, useLayoutEffect, useEffect} from "react";
import Frame from "./Frame";
import Plane from "./Plane";
import Vertex from "./Vertex";
import selectModeStore from "../../state/selectMode"

export const cubeBasePlane = {
    'F1': {position: [0, 0, 0], rotation: [0, Math.PI, 0]},
    'F2': {position: [0, 0.5, 0.5], rotation: [-Math.PI / 2, 0, 0]},
    'F3': {position: [-0.5, 0, 0.5], rotation: [0, -Math.PI / 2, 0]},
    'F4': {position: [0, -0.5, 0.5], rotation: [Math.PI / 2, 0, 0]},
    'F5': {position: [0.5, 0, 0.5], rotation: [0, Math.PI / 2, 0]},
    'F6': {position: [0, 0, 1], rotation: [0, 0, 0]}
}


export default function Cube(props) {
    const mesh = useRef();
    const [clicked, setClicked] = useState(false);
    const [disable, setDisable] = useState(selectModeStore.initialState);

    useLayoutEffect(() => {
        // selectModeStore.subscribe(setDisable)
        selectModeStore.flush()
    })

    useEffect(() => {
        const sub = selectModeStore.subscribe(setDisable)
        disable.selectMode != 'volumeMode' ? setClicked(false) : undefined
        return () => sub.unsubscribe()
    },[disable])

    return (
        // The mesh is at the origin
        // Math.since it is inside a group, it is at the origin
        // of that group
        // It's rotated by 90 degrees along the X-axis
        // This is because, by default, planes are rendered
        // in the X-Y plane, where Y is the up direction
        <group {...props} ref={mesh}
        onClick={(e) => {if(disable.selectMode == 'volumeMode'){e.stopPropagation(), setClicked(!clicked)}}}
        >
            {/*Front Plane with Frame*/}
            <Frame position={[0.5, 0, 0]} rotation={[0, Math.PI / 4, 0]} name={{...props.name,frame:{type:'1'}}}/>
            <Frame position={[-0.5, 0, 0]} rotation={[0, Math.PI / 4, 0]} name={{...props.name,frame:{type:'2'}}}/>
            <Frame position={[0, 0.5, 0]} rotation={[Math.PI / 4, 0, Math.PI / 2]} name={{...props.name,frame:{type:'3'}}}/>
            <Frame position={[0, -0.5, 0]} rotation={[Math.PI / 4, 0, Math.PI / 2]} name={{...props.name,frame:{type:'4'}}}/>
            {/*F1*/}
            <Plane position={cubeBasePlane.F1.position} rotation={cubeBasePlane.F1.rotation} scale={[1, 1, 1]} color={clicked ? 'lightblue' : 'pink'} name={{...props.name,plane:{type:'F1'}}}/>
            {/*Up Plane with Frame*/}
            <Frame position={[0.5, 0.5, 0.5]} rotation={[Math.PI / 2, Math.PI / 4, 0]} scale={[1, 1, 1]} name={{...props.name,frame:{type:'5'}}}/>
            <Frame position={[-0.5, 0.5, 0.5]} rotation={[Math.PI / 2, Math.PI / 4, 0]} scale={[1, 1, 1]} name={{...props.name,frame:{type:'6'}}}/>
            <Frame position={[0, 0.5, 1]} rotation={[Math.PI / 4, 0, Math.PI / 2]} scale={[1, 1, 1]} name={{...props.name,frame:{type:'7'}}}/>
            {/*F2*/}
            <Plane position={cubeBasePlane.F2.position} rotation={cubeBasePlane.F2.rotation} scale={[1, 1, 1]} color={clicked ? 'lightblue' : 'red'} name={{...props.name,plane:{type:'F2'}}}/>
            {/*Back Plane with Frame*/}
            <Frame position={[0.5, 0, 1]} rotation={[0, Math.PI / 4, 0]} scale={[1, 1, 1]} name={{...props.name,frame:{type:'8'}}}/>
            <Frame position={[-0.5, 0, 1]} rotation={[0, Math.PI / 4, 0]} scale={[1, 1, 1]} name={{...props.name,frame:{type:'9'}}}/>
            <Frame position={[0, -0.5, 1]} rotation={[Math.PI / 4, 0, Math.PI / 2]} scale={[1, 1, 1]} name={{...props.name,frame:{type:'10'}}}/>
            {/*F6*/}
            <Plane position={cubeBasePlane.F6.position} rotation={cubeBasePlane.F6.rotation} scale={[1, 1, 1]} color={clicked ? 'lightblue': 'orange'} name={{...props.name,plane:{type:'F6'}}}/>
            {/*Base Plane with Frame*/}
            <Frame position={[0.5, -0.5, 0.5]} rotation={[Math.PI / 2, Math.PI / 4, 0]} scale={[1, 1, 1]} name={{...props.name,frame:{type:'11'}}}/>
            <Frame position={[-0.5, -0.5, 0.5]} rotation={[Math.PI / 2, Math.PI / 4, 0]} scale={[1, 1, 1]} name={{...props.name,frame:{type:'12'}}}/>
            {/*F4*/}
            <Plane position={cubeBasePlane.F4.position} rotation={cubeBasePlane.F4.rotation} scale={[1, 1, 1]} color={clicked ? 'lightblue':'blue'} name={{...props.name,plane:{type:'F4'}}}/>
            {/*Right Plane*/}
            {/*F5*/}
            <Plane position={cubeBasePlane.F5.position} rotation={cubeBasePlane.F5.rotation} scale={[1, 1, 1]} color={clicked ? 'lightblue':'green'} name={{...props.name,plane:{type:'F5'}}}/>
            {/*Left Plane*/}
            {/*F3*/}
            <Plane position={cubeBasePlane.F3.position} rotation={cubeBasePlane.F3.rotation} scale={[1, 1, 1]} color={clicked ? 'lightblue':'purple'} name={{...props.name,plane:{type:'F3'}}}/>
            {/* Vertex */}
            <Vertex position={[0.5, 0.5, 0]} color={'#A7A29A'} name={{...props.name,vertex:{type:'1'}}}/>
            <Vertex position={[-0.5, 0.5, 0]} color={'#A7A29A'} name={{...props.name,vertex:{type:'2'}}}/>
            <Vertex position={[-0.5, -0.5, 0]} color={'#A7A29A'} name={{...props.name,vertex:{type:'3'}}}/>
            <Vertex position={[0.5, -0.5, 0]} color={'#A7A29A'} name={{...props.name,vertex:{type:'4'}}}/>
            <Vertex position={[-0.5, -0.5, 1]} color={'#A7A29A'} name={{...props.name,vertex:{type:'5'}}}/>
            <Vertex position={[0.5, -0.5, 1]} color={'#A7A29A'} name={{...props.name,vertex:{type:'6'}}}/>
            <Vertex position={[0.5, 0.5, 1]} color={'#A7A29A'} name={{...props.name,vertex:{type:'7'}}}/>
            <Vertex position={[-0.5, 0.5, 1]} color={'#A7A29A'} name={{...props.name,vertex:{type:'8'}}}/>
        </group>
    );
}
