import {useState,useRef, useLayoutEffect, useEffect} from "react";
import {useFrame} from "@react-three/fiber/native";
// import {useCursor} from "@react-three/drei/native";
import messageStore from "../../state/store"
import selectStore from "../../state/selectStore"
import selectModeStore from "../../state/selectMode"
import apiStore from "../../state/apiStore"

export default function Frame(props) {
    // const texture = new TextureLoader().load(
    //     'https://static.vecteezy.com/system/resources/thumbnails/007/408/014/small/stylish-panoramic-background-silver-steel-metal-texture-vector.jpg'
    // );
    const ref = useRef()
    const [hovered, setHover] = useState(false);
    const [clicked, setClicked] = useState(false)
    const [clickObj, setClickObj] = useState(selectStore.initialState)
    const [disable, setDisable] = useState(selectModeStore.initialState);
    // useFrame((state) => ref.current.scale.setScalar(hovered ? 1 + Math.sin(state.clock.elapsedTime * 10) / 50 : 1))
    // useCursor(hovered)

    const handleClick = (e) => {
        e.stopPropagation()
        setClicked(!clicked)
        const clicked = {...props.name.frame, ...{position:props.position, rotation: props.rotation}}
        const clickObject = {...props.name,frame: clicked}
        // messageStore.send(clickObject)
        selectStore.send(clickObject)
        // apiStore.send(clickObject)
    }

    useLayoutEffect(() => {
        // selectStore.subscribe(setClickObj)
        // selectStore.flush()
        // selectModeStore.subscribe(setDisable)
        // selectModeStore.flush()
    })

    useEffect(() => {
        const sub = selectModeStore.subscribe(setDisable)
        const sub2 = selectStore.subscribe(setClickObj)
        clickObj.data.length == 0 || disable.selectMode != 'distanceMode' ? setClicked(false) : undefined
        return () => {sub.unsubscribe(); sub2.unsubscribe()}
    },[clickObj, disable])

    return (
        <mesh
            {...props}
            ref={ref}
            onClick={(e) => {
                if(disable.selectMode == 'distanceMode'){
                    handleClick(e)
                }
            }
        }
            // onPointerOver={(e) => (e.stopPropagation(), setHover(true))}
            // onPointerOut={(e) => setHover(false)}
        >
            <cylinderGeometry args={[0.05, 0.05, 1, 4, 1, true]}/>
            <meshStandardMaterial color={clicked ? 'aquamarine' : '#A7A29A'} 
                                // metalness={0} 
                                // roughness={0}
                            //    map={texture}
                               opacity={0.5}/>
        </mesh>
    )
}
