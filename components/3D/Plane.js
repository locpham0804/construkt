import {useRef, useState, useLayoutEffect, useEffect} from "react";
import {useFrame} from "@react-three/fiber/native";
// import {useCursor} from "@react-three/drei/native";
import messageStore from "../../state/store"
import selectStore from "../../state/selectStore"
import selectModeStore from "../../state/selectMode"
import apiStore from "../../state/apiStore"


export default function Plane(props) {
    const planeName = props.name
    // const texture = new TextureLoader().load(
    //     'https://cdn2.iconfinder.com/data/icons/arrows-part-1/32/tiny-arrow-top-left-2-512.png'
    // );
    const ref = useRef()
    const [hovered, setHover] = useState(false);
    const [clicked, setClicked] = useState(false);
    const [clickObj, setClickObj] = useState(selectStore.initialState);
    const [disable, setDisable] = useState(selectModeStore.initialState);
    // useFrame((state) => ref.current.scale.setScalar(hovered ? 1 + Math.sin(state.clock.elapsedTime * 10) / 50 : 1))
    // useCursor(hovered)

    const handleClick = (e) => {
        e.stopPropagation()
        setClicked(!clicked)
        const clickObject = {...props.name,...{plane:{position:props.position, rotation: props.rotation}}}
        // messageStore.send(clickObject)
        selectStore.send(clickObject)
        apiStore.send(clickObject)
    }

    useLayoutEffect(() => {
        // selectStore.subscribe(setClickObj)
        // selectStore.flush()
        // selectModeStore.subscribe(setDisable)
        // selectModeStore.flush()
    })

    useEffect(() => {
        const sub = selectModeStore.subscribe(setDisable)
        const sub2 = selectStore.subscribe(setClickObj)
        clickObj.data.length == 0 || (disable.selectMode != 'areaMode' && disable.selectMode != 'pointToFaceMode') ? setClicked(false) : undefined
        return () => {sub.unsubscribe(); sub2.unsubscribe()}
    },[clickObj, disable])

    return (
        <mesh
            {...props}
            ref={ref}
            onClick={(e) => {if(disable.selectMode == 'areaMode' || disable.selectMode == 'pointToFaceMode'){handleClick(e)}}}
            // onPointerOver={(e) => (e.stopPropagation(), setHover(true))}
            // onPointerOut={(e) => setHover(false)}
        >
            <boxGeometry args={[1, 1, 0.075]} />
            {/*<textGeometry args={['test',{font, size: 100, height: .00001}]}/>*/}
            <meshStandardMaterial 
                // opacity={1} 
                color={clicked ? 'lightblue' : props.color}
            />
            {/*<meshBasicMaterial color={props.color ? props.color : "white"}*/}
            {/*    map={texture}*/}
            {/*                   opacity={hovered ? 1 : 0.5} transparent="true" side={DoubleSide} deepWrite="false"/>*/}
        </mesh>

    );
}
