import {useState,useRef, useMemo, useLayoutEffect, useEffect } from "react";
import {useFrame} from "@react-three/fiber/native";
// import {useCursor} from "@react-three/drei/native";
import messageStore from "../../state/store"
import selectStore from "../../state/selectStore"
import selectModeStore from "../../state/selectMode"
import apiStore from "../../state/apiStore"


export default function Vertex(props) {
    // const texture = new TextureLoader().load(
    //     'https://static.vecteezy.com/system/resources/thumbnails/007/408/014/small/stylish-panoramic-background-silver-steel-metal-texture-vector.jpg'
    // );

    const ref = useRef()
    const [hovered, setHover] = useState(false);
    const [clicked, setClicked] = useState(false)
    const [clickObj, setClickObj] = useState(selectStore.initialState)
    const [disable, setDisable] = useState(selectModeStore.initialState);
    // useCursor(hovered)

    const handleClick = (e) => {
        e.stopPropagation()
        setClicked(!clicked)
        const clicked = {...props.name.vertex, ...{position:props.position}}
        const clickObject = {...props.name,vertex:clicked}
        // messageStore.send(clickObject)
        selectStore.send(clickObject)
        // apiStore.send(clickObject)
    }

    useLayoutEffect(() => {
        // selectStore.subscribe(setClickObj)
        // selectStore.flush()
        // selectModeStore.subscribe(setDisable)
        // selectModeStore.flush()
    })

    useEffect(() => {
        const sub = selectModeStore.subscribe(setDisable)
        const sub2 = selectStore.subscribe(setClickObj)
        clickObj.data.length == 0 || (disable.selectMode != 'distanceMode' && disable.selectMode !='pointToFaceMode') ? setClicked(false) : undefined
        return () => {sub.unsubscribe(); sub2.unsubscribe()}
    }, [clickObj, disable])

    return (
        <mesh
            {...props}
            ref={ref}
            onClick={(e) => {if(disable.selectMode == 'distanceMode' || disable.selectMode =='pointToFaceMode'){handleClick(e)}}}
            // onPointerOver={(e) => (e.stopPropagation(), setHover(true))}
            // onPointerOut={(e) => setHover(false)}
        >
            <sphereGeometry args={[0.045, 64, 64, 0 , 2* Math.PI, 0, Math.PI]}/>   
            <meshStandardMaterial 
                metalness={0} 
                roughness={0} 
                opacity={0.4}
                transparent
                color={clicked ? 'blue' : props.color} 
            />
        </mesh>
    )
}
