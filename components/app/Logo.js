import React from 'react'
import { Image, StyleSheet } from 'react-native'

export default function Logo() {
  return <Image source={require('../../assets/logo.jpg')} style={styles.image} />
}

const styles = StyleSheet.create({
  image: {
    height:110,
    aspectRatio: 3, 
    resizeMode: 'contain',
  },
})
