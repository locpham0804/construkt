export function arrayAdd(arr1, arr2) {
    const arr = []
    for (let i = 0; i < arr1.length; i++) {
        arr[i] = arr1[i] + arr2[i]
    }
    return arr
}

export function arraySubtract(arr1, arr2) {
    const arr = []
    for (let i = 0; i < arr1.length; i++) {
        arr[i] = arr1[i] - arr2[i]
    }
    return arr
}
