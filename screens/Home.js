import {View, Image, Text} from 'react-native';



function Home({navigation}) {


    return (
        <View style={{flex: 1, alignSelf: 'stretch', backgroundColor: '#ffffff'}}>

            <Image
                // style={styles.tinyLogo}
                source={{
                    uri: "https://www.construkts.com/uploads/2/7/0/8/27088277/header_images/1402716579.jpg"
                }}
                style={{flex: 1, width: null, height: null, resizeMode: 'contain'}}
            />
        </View>
    )
}

export default Home
