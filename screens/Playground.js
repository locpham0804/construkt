import React, { useRef, useState, useLayoutEffect } from 'react';
import { Canvas, useFrame } from '@react-three/fiber/native';
import { LogBox, View, StyleSheet, Pressable, ScrollView } from 'react-native';
// import {OrbitControls} from '@react-three/drei/native';
import useControls from "r3f-native-orbitcontrols"
import LShape, { LShapeBaseCube } from '../components/3D/BlockL'
import { RadioButton, Text, List, Modal, Portal, Provider, Button } from 'react-native-paper';
import messageStore from "../state/store";
import selectModeStore from "../state/selectMode"
import apiStore from "../state/apiStore"

// TODO: Remove when fixed
LogBox.ignoreLogs([
  (window.performance.clearMeasures = () => { }),
  (window.performance.clearMarks = () => { }),
  (window.performance.measure = () => { }),
  (window.performance.mark = () => { }),
]);

const selectMode = [
  {
    id: 1,
    label: 'Area Mode',
    value: 'areaMode'
  },
  {
    id: 2,
    label: 'Distance Mode',
    value: 'distanceMode'
  },
  {
    id: 3,
    label: 'Volume Mode',
    value: 'volumeMode'
  },
  {
    id: 4,
    label: 'Point To Face Mode',
    value: 'pointToFaceMode'
  },
]

const PlaygroundView = ({}) => {
  const [OrbitControls, events, camera] = useControls()

  

  return (
    <View {...events} style={{ flex:1, backgroundColor: "gray" }}>
        <Canvas camera={camera}>
          <OrbitControls maxPolarAngle={Math.PI/2} enableZoom={true} />
          <ambientLight />
          <pointLight position={[10, 10, 10]} />
          <LShape position={[0, 0, 0]} rotation={[0, Math.PI / 2, 0]} />

        </Canvas>
      </View>
  )
}

const ControlMode = ({}) => {
  const [modalVisible, setModalVisible] = useState(false)

  const [state, setState] = useState({
    detailMode: 'simple',
    distanceMode: false,
    areaMode: true,
    volumeMode: false,
    pointToFaceMode: false
  })
  const [selectMode, setSelectMode] = React.useState('areaMode');
  const [visible, setVisible] = React.useState(false);
  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);

  const handleSelectMode = (value) => {
    setSelectMode(value)
    const selectMode = {selectMode : value}
    selectModeStore.send(selectMode)
    hideModal()
  }

  return (
    <View style={{ position: 'absolute', top: 5, zIndex: 100, width:'100%' }}>
      <Provider >
        <Portal >
          <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={styles.modalStyle} style={{ alignItems: 'center' }}>
            <RadioButton.Group onValueChange={newValue => {handleSelectMode(newValue)}} value={selectMode}>
              <View style={styles.listItem}>
                <RadioButton value="areaMode"/>
                <Text>Area Mode</Text>
              </View>
              <View style={styles.listItem}>
                <RadioButton value="distanceMode"/>
                <Text>Distance Mode</Text>
              </View>
              <View style={styles.listItem}>
                <RadioButton value="volumeMode"/>
                <Text>Volume Mode</Text>
              </View>
              <View style={styles.listItem}>
                <RadioButton value="pointToFaceMode" />
                <Text>Point To Face Mode</Text>
              </View>
            </RadioButton.Group>
          </Modal>
        </Portal>
        <Button style={{ marginTop: 5 }} onPress={showModal}>
          Show
        </Button>
      </Provider>
    </View>  
  )
}

const InfoView = ({}) => {
  const [clickObj, setClickObj] = useState(messageStore.initialState)
  const [WolframResult, setWolframResult] = useState(apiStore.initialState)

  
  useLayoutEffect(() => {
    messageStore.subscribe(setClickObj)
    messageStore.flush()
    apiStore.subscribe(setWolframResult)
    apiStore.flush()  
}, [])


  return (
    <View style={{ position: 'absolute', bottom: 40, zIndex: 100, width:'100%', alignItems:'center', backgroundColor: 'white', padding: 8, textAlign:'center' }}>
                <Text style={styles.text_header}>Wolfram API Result</Text>
                <ScrollView 
                    style={[styles.resultBox, {
                    borderColor: '#000000',
                    borderWidth: 1,
                    marginTop: 15,
                    height:50,
                    alignContent:'center',
                }]}>

                    {/* {clickObj.data.map((message,i)=>(
                    <Text style={{textAlign:'center'}} key={i}>{message.shape.type}</Text>
                    ))} */}
                    {WolframResult.data.map((message,i)=>(
                        <Text style={{textAlign:'center'}} key={i}>{message.type}: {message.setup}-{message.punchline}</Text>
                    ))}
                  
                    
                </ScrollView>
            </View>
  )
}

export default function App() {


  return (
    <View style={{height:'100%'}}>
      <ControlMode />
      <PlaygroundView/>
      <InfoView />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.1)"
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
    alignItems: 'center',
  },
  modalStyle:{
    backgroundColor: 'white', 
    padding: 10,
    zIndex: 100,
    position:'absolute',
    top: 10,
    width: '50%',
    textAlign:'center'
  },
  text_header: {
    fontWeight: 'bold',
    fontSize: 16
},
resultBox:{
  width: '100%',
  minHeight:50,
  height: "auto", 
  maxHeight:200,
  borderRadius: 10
},
});
