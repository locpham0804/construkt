import SignInScreen from './SignInScreen';
import { createStackNavigator } from '@react-navigation/stack'


const Stack = createStackNavigator()

const RootStackScreen = ({navigation}) => (
    <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="LoginScreen" component={SignInScreen} />
    </Stack.Navigator>
)

export default RootStackScreen; 