import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    TextInput,
    Platform,
    StyleSheet ,
    StatusBar,
} from 'react-native';
import * as Animatable from 'react-native-animatable';

import { useTheme } from 'react-native-paper';

import { AuthContext } from '../components/context';

import Logo from '../components/app/Logo'

import {useAuth0, Auth0Provider} from 'react-native-auth0';



const SignInScreen = () => {

    const {authorize, clearSession, user} = useAuth0();

    const { signIn } = React.useContext(AuthContext);

    const { trialLogin } = React.useContext(AuthContext)

    const loginHandle = (userLoginInfo) => {
        signIn(userLoginInfo);
    }

    const _loginWithAuth0 = async () => {
        try {
            await authorize({scope: 'openid profile email'});
            console.log(user)
            loginHandle(user)
          } catch (e) {
            console.log(e);
          }
    };

    const _trialLogin = async () => {
        console.log('123')
        trialLogin()
    }

    const { colors } = useTheme();

    return (
      <View style={styles.container}>

          <StatusBar backgroundColor='#009387' barStyle="light-content"/>
        <View style={styles.header}>
        <Logo/>
            <Text style={styles.text_header}>Welcome!</Text>
        </View>
        <Animatable.View
            animation="fadeInUpBig"
            style={[styles.footer, {
                backgroundColor: colors.background
            }]}
        >
            <Text style={[styles.text_footer, {
                color: colors.text
            }]}>Welcome to Construkt</Text>
            <View style={styles.button}>
                <TouchableOpacity
                    style={[styles.signIn,{
                        backgroundColor:'#01ab9d',
                        borderColor: '#009387',
                        borderWidth: 1,
                        position:'absolute',
                        marginTop: 15
                    }]}
                    onPress={() => _loginWithAuth0()}
                >
                    <Text style={[styles.textSign, {
                        color:'#fff'
                    }]}>Sign In</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => trialLogin()}
                    style={[styles.signIn, {
                        borderColor: '#009387',
                        borderWidth: 1,
                        marginTop: 80,
                        position:'absolute',
                        zIndex:100
                    }]}
                >
                    <Text style={[styles.textSign, {
                        color: '#009387'
                    }]}>Sign In as Anonymous</Text>
                </TouchableOpacity>
            </View>
        </Animatable.View>
      </View>
    );
};

export default SignInScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#009387'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    }
  });