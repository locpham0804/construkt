import {Subject} from 'rxjs';
import produce from 'immer';
import {api} from '../api';

const subject = new Subject();

const initialState = {
  message: [],
  data: [],
  newDataCount: 0,
};

let state = initialState;

const apiStore = {
  flush: () => {
    state = produce(state, draft => {
      draft.newDataCount = 0;
    });
    subject.next(state);
  },
  subscribe: setState => subject.subscribe(setState),
  send: async message => {
    const response = await api.testApi();
    if (response.data) {
      state = {
        ...state,
        message: [...state.message, message],
        data: [...state.data, response.data],
        newDataCount: state.newDataCount + 1,
      };
      subject.next(state);
    }
  },
  clear: () => {
    state = initialState;
    subject.next(state);
  },
  initialState,
};

export default apiStore;
