import {Subject} from 'rxjs';

const subject = new Subject();

import produce from 'immer';

const initialState = {
  data: [],
  newDataCount: 0,
};

let state = initialState;

const bluetoothState = {
  flush: () => {
    state = produce(state, draft => {
      draft.newDataCount = 0;
    });
    subject.next(state);
  },
  subscribe: setState => subject.subscribe(setState),
  send: message => {
    state = produce(state, draft => {
      draft.data.push(message);
      draft.newDataCount++;
    });
    subject.next(state);
  },
  clear: () => {
    state = initialState;
    subject.next(state);
  },
  initialState,
};

export {bluetoothState};
