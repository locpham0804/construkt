import { Subject } from "rxjs";
import produce from "immer";

const subject = new Subject();

const initialState = {
        selectMode: 'areaMode'
  }; 

let state = initialState;

const selectModeStore = {
    flush: ()=> {
        state = produce(state, (draft) => {
            draft = {
                selectMode: 'areaMode'
            };
        });
        subject.next(state);
    },
    subscribe: (setState) => subject.subscribe(setState),
    send: (message) => {
        state = produce(state, (draft) => {
            Object.assign(draft,message)
        });
        console.log(message)
        console.log(state)
        subject.next(state);
    },
    clear: () =>{
        state = initialState;
        subject.next(state);
    },
    initialState
}

export default selectModeStore