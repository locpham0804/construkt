import {Subject} from 'rxjs';
import produce from 'immer';

const subject = new Subject();

const initialState = {
  data: [],
  newDataCount: 0,
};

let state = initialState;

const messageStore = {
  flush: () => {
    state = produce(state, draft => {
      draft.newDataCount = 0;
    });
    subject.next(state);
  },
  subscribe: setState => subject.subscribe(setState),
  send: message => {
    state = produce(state, draft => {
      draft.data.push(message);
      draft.newDataCount++;
    });
    subject.next(state);
  },
  clear: () => {
    state = initialState;
    subject.next(state);
  },
  initialState,
};

export default messageStore;
